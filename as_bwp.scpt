tell application "Finder"
	if exists POSIX file "/Users/Shared/.labtech/receipts/bwp.inst" then
		display dialog "The black and white printer preset is already installed. If you still don't see the preset while printing please contact service@it360.biz" buttons {"Cancel"} default button 1
	else if exists POSIX file "/Users/Shared/.labtech/logs/bwp.inst" then
		display dialog "The black and white printer preset is already installed. If you still don't see the preset while printing please contact service@it360.biz" buttons {"Cancel"} default button 1
	end if
end tell

do shell script "curl -L -o /tmp/lt_folder.py 'https://bitbucket.org/it360/python/raw/master/modules/lt_folder.py'"

do shell script "python /tmp/lt_folder.py"

do shell script "rm /tmp/lt_folder.py"

do shell script "curl -L -o /Users/Shared/.labtech/scripts/bwp.sh 'https://bitbucket.org/it360/bash/raw/master/bwp.sh'"

do shell script "chmod +x /Users/Shared/.labtech/scripts/bwp.sh"

do shell script "/Users/Shared/.labtech/scripts/bwp.sh"

do shell script "rm /Users/Shared/.labtech/scripts/bwp.sh"

display dialog "Finished installing the black and white preset. You may need to restart your computer for the preset to become available in some applications." buttons {"Quit"} default button 1