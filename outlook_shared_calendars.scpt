-- This is a janky ass script that *should* go through a list of resource rooms or shared calendars and open them in outlook

-- It relies heavily (almost exclusively) on key codes to scroll through the menus and click things

-- This script is seriously janky, use at your own risk

on add_room(room)
	tell application "System Events"
		tell application "Microsoft Outlook"
			activate
		end tell
		# get through the menu
		delay 0.25
		key code 44 using {command down, shift down}
		delay 0.25
		repeat 4 times
			key code 124
			delay 0.05
		end repeat
		key code 31
		delay 0.25
		key code 124
		delay 0.25
		key code 125
		delay 0.25
		-- opens file > open > open user's folder...
		key code 36
		delay 1
		-- searches for the room
		keystroke room
		delay 0.25
		key code 36
		-- waits for the room popup
		delay 10
		-- selects the room
		key code 48
		delay 0.25
		key code 36
		-- waits for the redirect popup
		delay 15
		-- goes through the redirect popup
		repeat 3 times
			key code 48
			delay 0.05
		end repeat
		delay 0.25
		key code 49
		delay 0.25
		repeat 2 times
			key code 48
			delay 0.05
		end repeat
		delay 0.25
		key code 49
		delay 1
	end tell
end add_room

set rooms to {"room", "list", "here"}

repeat with room in rooms
	add_room(room)
end repeat