set the_opening to "A technician from IT360 is trying to rebuild your Outlook database. Please click OK and save out of any open Outlook windows.

Microsoft Outlook, Database Daemon, and Office Reminders will be quit.

All other Microsoft programs shouldn't be affected."

set the_closing to "Your Outlook database is being rebuilt.

Please don't open Outlook until the Done button is highlighted in the Microsoft Database Utility program.

If you have anymore issues after the rebuild is complete or any questions please contact us at: service@it360.biz or 309.282.3830"

set the_error to "Something went wrong with the pre-rebuild script. Please contact us at: service@it360.biz or 309.282.3830 to let us know."

set appPath to (path to applications folder as text) & "Microsoft Office 2011:Microsoft Outlook.app"

tell application "Finder"
	if exists appPath then
		null
	else
		tell me to quit
	end if
end tell

on open_programs(the_program, the_error)
	tell application "System Events"
		set the_process to name of processes
	end tell
	if the_process contains the_program then
		tell application the_program
			tell application "System Events" to set frontmost of process the_program to true
			quit
		end tell
	else if the_process does not contain the_program then
		null
	else
		display dialog the_error buttons {"OK"} default button 1
		tell me to quit
	end if
end open_programs

on ms_rebuild()
	tell application "Microsoft Database Utility"
		activate
		rebuild "Main Identity"
		activate
		delay 2
	end tell
end ms_rebuild

on dialog(the_text)
	do shell script "sw_vers -productVersion | cut -d'.' -f2"
	set osver to result
	if osver is less than "9" then
		null
	else if osver is greater than or equal to "9" then
		display dialog the_text buttons {"OK"} default button 1
	end if
end dialog



dialog(the_opening)
open_programs("Microsoft Outlook", the_error)
open_programs("Microsoft Database Daemon", the_error)
open_programs("Microsoft Office Reminders", the_error)
ms_rebuild()
tell me to activate
dialog(the_closing)